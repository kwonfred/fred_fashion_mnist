#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 9 15:53:09 2018

@author: fred
"""

import torch.nn as nn

# Convolutional neural network
class ConvNet(nn.Module):

    """ConvNet defines 2 layer convlutional network for Fashion-MNIST analysis.

    ConvNet has 2 convolutional layers, each with batch normalization,
    rectified linear unit, and pooling. Then a fully connected (fc) layer 
    applies a linear transformation to the incoming data.
    """

    def __init__(self, num_classes):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
            )
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2)
            )
        self.fc1 = nn.Linear(1568, num_classes)
        # 1568 calculated using print(x.shape) below;
        # how can I compute correct dimensions without trial and error?

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = x.reshape(x.size(0), -1)
        #print(x.shape) briefly used to calculate size for fc1 above
        x = self.fc1(x)
        return x
