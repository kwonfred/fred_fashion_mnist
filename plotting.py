#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 9 16:04:11 2018

@author: fred
"""

# Computing and Plotting Confusion Matrix
# modified from http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py

import os
import itertools
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
from sklearn.metrics import confusion_matrix

if not os.path.exists('plots'):
    os.makedirs('plots')

# Define plot confusion matrix function
def plot_confusion_matrix(labels, predicted, classes, normalize, title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    Inputs labels and predicted are tensors with class for each example.
    """

    # Compute confusion Matrix
    cm = confusion_matrix(labels, predicted)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.figure(figsize=(6, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=60)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    if normalize:
        plt.savefig('plots/Confusion Matrix, with normalization.png', dpi=100)
    else:
        plt.savefig('plots/Confusion Matrix, without normalization.png', dpi=100)

# Define plot loss
def plot_loss(epoch_train_losses, epoch_test_losses, learning_rate):
    """
    This function plots the loss during training and testing.
    """
    epochs = np.arange(len(epoch_train_losses)) + 1
    plt.figure(figsize=(6, 6))

    # For integer x-axis
    locator = matplotlib.ticker.MultipleLocator(1)
    plt.gca().xaxis.set_major_locator(locator)
    formatter = matplotlib.ticker.StrMethodFormatter("{x:.0f}")
    plt.gca().xaxis.set_major_formatter(formatter)

    plt.plot(epochs, np.squeeze(epoch_train_losses), label='Train Loss')
    plt.plot(epochs, np.squeeze(epoch_test_losses), label='Test Loss')
    plt.legend()
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.title("Learning rate =" + str(learning_rate))
    plt.savefig('plots/Train and Test Loss vs Epoch.png', dpi=100)
