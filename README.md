## fred_fashion_mnist
This repository was made to learn code review with AI-Sinai Group (Anthony Costa, Eric Oermann)

## Table of Contents
#### 1. Check out the [master branch](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/)
#### 2. [run_convnet.py](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/run_convnet.py)
* Imports Fashion-MNIST built into torchvision, saves to [data] directory (not included in this repo due to .gitignore)
* Runs forward / back propagation on training set based on convolution network defined in [convnet.py](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/convnet.py)
* After each epoch, uses [convnet.py](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/convnet.py) to evaluate the test set
* Saves model parameters in [model.ckpt](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/model.ckpt)
* Uses [plotting.py](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/plotting.py) to plot confusion matrix and train/test loss vs epoch 
#### 3. [convnet.py](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/convnet.py)
* Defines network that consists of 2 Convolution layers (Conv --> Batch Normalization --> ReLU --> Pooling) and subsequent 1 FC layer
#### 4. [plotting.py](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/plotting.py)
* Contains 1) plot_confusion_matrix that computes confusion matrix using sklearn.metrics and plots it and 2) plot_loss to plot training loss
* Plots to [Confusion matrix](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/plots/Confusion%20Matrix%2C%20with%20normalization.png) and [Loss vs Epoch](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/plots/Train%20and%20Test%20Loss%20vs%20Epoch.png) in the [plots](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/plots/) directory
#### 5. Images of [Plots](https://bitbucket.org/kwonfred/fred_fashion_mnist/src/master/plots/)
![Normalized Confusion Matrix](https://bitbucket.org/kwonfred/fred_fashion_mnist/raw/faa6d82b6da67cf4cc67c1dc2d76480158ecdf54/plots/Confusion%20Matrix%2C%20with%20normalization.png)
![Learning Curve](https://bitbucket.org/kwonfred/fred_fashion_mnist/raw/faa6d82b6da67cf4cc67c1dc2d76480158ecdf54/plots/Train%20and%20Test%20Loss%20vs%20Epoch.png)

## Author
Fred Kwon - [bitbucket](https://bitbucket.org/kwonfred)