#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 1 13:08:50 2018

@author: fred
"""

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
from convnet import ConvNet
from plotting import plot_confusion_matrix, plot_loss
#should check if scikit-learn is fine to use

# Device configuration
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

# Define hyper parameters
learning_rate = 0.01
num_epochs = 5
batch_size = 100
num_classes = 10 #number of classes in Fasion MNIST
test_size = 10000 #made 10000 to make it easier to create Confusion Matrix

# Import Fashion MNIST dataset to root_directoy/data folder
# https://pytorch.org/docs/master/torchvision/datasets.html#fashion-mnist
trainset = torchvision.datasets.FashionMNIST(root='./data/',
                                             train=True,
                                             download=True,
                                             transform=transforms.ToTensor())
testset = torchvision.datasets.FashionMNIST(root='./data/',
                                            train=False,
                                            download=True,
                                            transform=transforms.ToTensor())

# Load Fashion MNIST dataset
trainloader = torch.utils.data.DataLoader(trainset,
                                          batch_size=batch_size,
                                          shuffle=True)
testloader = torch.utils.data.DataLoader(testset,
                                         batch_size=test_size,
                                         shuffle=False)
classes = ('T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
           'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot')

# Run convolutional network
model = ConvNet(num_classes)

# Loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
# options for optimizer: SGD, Nesterov-SGD, Adam, RMSProp

# Train the model
total_step = len(trainloader)
epoch_train_losses = [] #to keep track of train loss per epoch
epoch_test_losses = [] #to keep track of test loss per epoch
for epoch in range(num_epochs):
    for i, data in enumerate(trainloader):
        images, labels = data
        losses = [] #to keep track of loss

        # Forward pass
        outputs = model(images)
        loss = criterion(outputs, labels)
        losses.append(loss) #to plot loss later

        # Backward pass and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        #print after every 100 steps (i.e. 100 * batch_size training examples)
        if (i+1) % 100 == 0:
            print('Epoch [{}/{}], Step [{}/{}], Loss: {:.3f}'
                  .format(epoch+1, num_epochs, i+1, total_step, loss.item()))

    # Compute and store training loss per epoch
    avg_train_loss = sum(losses)/len(losses)
    epoch_train_losses.append(avg_train_loss)

    # Test the model per epoch
    class_correct = list(0. for i in range(10)) #initialize to 0
    class_total = list(0. for i in range(10))   #initialize to 0
    with torch.no_grad():
        for data in testloader: #goes through 10000 / test_size times
            images, labels = data
            outputs = model(images) #forward pass to make test prediction

            # Compute and store test loss per epoch
            test_loss = criterion(outputs, labels)
            epoch_test_losses.append(test_loss)

            _, predicted = torch.max(outputs, 1) #labels has length: test_size
            c = (predicted == labels).squeeze()
            for i in range(test_size):
                label = labels[i] #labels has length: test_size
                class_correct[label] += c[i].item()
                class_total[label] += 1

    # Print overall accuracy per epoch
    print('Epoch [{}/{}], Test Accuracy of model on the {} test images: {} %'
          .format(epoch +1, num_epochs, int(sum(class_total)),
                  100 * sum(class_correct) / sum(class_total)))

    # Print accuracy for each class per epoch
    for i in range(10):
        print('Epoch [{}/{}], Accuracy of {}: {} %'
              .format(epoch +1, num_epochs, classes[i],
                      100 * class_correct[i] / class_total[i]))

# Save the model checkpoint
torch.save(model.state_dict(), 'model.ckpt')

#%% Plotting Confusion Matrix and Learning Curve

# Plot normalized and non-normalized confusion matrix
plot_confusion_matrix(labels, predicted, classes, normalize=False,
                      title='Confusion matrix, without normalization')
plot_confusion_matrix(labels, predicted, classes, normalize=True,
                      title='Confusion matrix, with normalization')

# Plot Loss vs Iteration
plot_loss(epoch_train_losses, epoch_test_losses, learning_rate)
